import 'package:flutter/material.dart';

Widget errorMessage(Size size, String message) {
  return Container(
    // height: size.height,
    // width: size.width,
    alignment: Alignment.center,
    child: Text(
      message,
      style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700,color: Colors.red),
    ),
  );
}
