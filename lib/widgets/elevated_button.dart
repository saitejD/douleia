import 'package:flutter/material.dart';

Widget elevatedButton(BuildContext context, String title, Function() onPress) {
  return ElevatedButton(
    style: ElevatedButton.styleFrom(
      primary: Theme.of(context).primaryColor,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      minimumSize: Size(double.maxFinite, 56),
    ),
    onPressed: onPress,
    child: Text(
      title,
      style: TextStyle(
          color: Colors.white, fontWeight: FontWeight.w600, fontSize: 22),
    ),
  );
}
