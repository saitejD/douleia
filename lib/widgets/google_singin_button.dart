// ignore_for_file: prefer_const_literals_to_create_immutables

import 'package:douleia/firebase_services/authentication_service.dart';
import 'package:douleia/screens/employee/employee_details.dart';
import 'package:douleia/screens/employee/employee_home.dart';
import 'package:douleia/screens/employer/employer_details.dart';
import 'package:douleia/screens/home.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class GoogleSignInButton extends StatefulWidget {
  final String groupValue;
  final BuildContext context;
  const GoogleSignInButton(
      {Key? key, required this.groupValue, required this.context})
      : super(key: key);

  @override
  _GoogleSignInButtonState createState() => _GoogleSignInButtonState();
}

class _GoogleSignInButtonState extends State<GoogleSignInButton> {
  bool _isSigningIn = false;

  @override
  Widget build(BuildContext context) {
    return _isSigningIn
        ? const CircularProgressIndicator(
            // valueColor: AlwaysStoppedAnimation<Color>(),
            )
        : OutlinedButton(
            style: OutlinedButton.styleFrom(
              backgroundColor: Colors.white,
              minimumSize: Size(56, 56),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(40),
              ),
            ),
            onPressed: () async {
              setState(() {
                _isSigningIn = true;
              });
              String? dp = await AuthenticationService()
                  .singingInWithGoogle(widget.context);

              setState(() {
                _isSigningIn = false;
              });
              if (dp != null) {
                print(widget.groupValue);
                Navigator.pushAndRemoveUntil(
                    context,
                    CupertinoPageRoute(
                        builder: (context) => widget.groupValue == 'employee'
                            ? EmployeeDetailsScreen(profilePic: dp)
                            : EmployerDetailsScreen(dp: dp,)),
                    (route) => false);
              }
            },
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FaIcon(FontAwesomeIcons.google),
                  const Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Text(
                      'Sign up with Google',
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
  }
}
