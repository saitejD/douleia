import 'package:fluttertoast/fluttertoast.dart';

Future<bool?> toastMessage(String message) async {
  return Fluttertoast.showToast(
    msg: message,
    toastLength: Toast.LENGTH_SHORT,
    gravity: ToastGravity.BOTTOM,
  );
}
