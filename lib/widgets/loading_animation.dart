import 'package:flutter/material.dart';

Widget loading() {
  return Center(
    child: CircularProgressIndicator(),
  );
}

Widget loadingList() {
  return SizedBox(
    height: 300,
    child: ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: 5,
      itemBuilder: (context, index) => Container(
        height: 300,
        width: 400,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
        ),
        margin: EdgeInsets.only(right: 16, top: 10, bottom: 10),
        padding: EdgeInsets.all(16),
      ),
    ),
  );
}
