List<dynamic> categories = [
  {
    "name": "Engineer",
    "asset": "assets/categories/engineer.png",
  },
  {
    "name": "Doctor",
    "asset": "assets/categories/doctor.png",
  },
  {
    "name": "Chef",
    "asset": "assets/categories/cooker.png",
  },
  {
    "name": "Farmer",
    "asset": "assets/categories/farmer.png",
  },
  {
    "name": "Teacher",
    "asset": "assets/categories/teacher.png",
  },
  {
    "name": "Mechanic",
    "asset": "assets/categories/mechanic.png",
  },
  {
    "name": "Labour",
    "asset": "assets/categories/labour.png",
  },
  {
    "name": "Contractor",
    "asset": "assets/categories/contractor.png",
  },
];
