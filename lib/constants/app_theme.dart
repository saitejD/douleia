import 'package:flutter/material.dart';

class AppThemeData {
  AppThemeData._();
  static final ThemeData themeData = ThemeData(
    scaffoldBackgroundColor: Color(0xFFF1F2F2),
    primaryColor: Color(0xFF1A1C27),
    fontFamily: "PT Serif",
    colorScheme: ColorScheme.light(
      primary: Color(0xFF1A1C27),
      secondary: Color(0xFFF1F2F2),
    ),
    inputDecorationTheme: InputDecorationTheme(
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Color(0xFF1A1C27)),
        borderRadius: BorderRadius.circular(10),
      ),
    ),
  );
}
