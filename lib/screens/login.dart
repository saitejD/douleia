import 'package:douleia/bloc/authentication_provider.dart';
import 'package:douleia/firebase_services/authentication_service.dart';
import 'package:douleia/screens/employee/employee_home.dart';
import 'package:douleia/screens/employer/employer_home.dart';
import 'package:douleia/screens/home.dart';
import 'package:douleia/screens/register.dart';
import 'package:douleia/widgets/email_validation.dart';
import 'package:douleia/widgets/google_singin_button.dart';
import 'package:douleia/widgets/loading_animation.dart';
import 'package:douleia/widgets/toastMessage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:proste_bezier_curve/proste_bezier_curve.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  bool obscurePasswd = true;
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  bool isLoading = false;
  String groupValue = "userType";

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        color: Theme.of(context).colorScheme.secondary,
        child: Stack(
          children: [
            /// Wavy line
            ClipPath(
              clipper: ProsteBezierCurve(
                position: ClipPosition.bottom,
                list: [
                  BezierCurveSection(
                    start: Offset(0, 125),
                    top: Offset(size.width / 4, 150),
                    end: Offset(size.width / 2, 125),
                  ),
                  BezierCurveSection(
                    start: Offset(size.width / 2, 125),
                    top: Offset(size.width / 4 * 3, 100),
                    end: Offset(size.width, 150),
                  ),
                ],
              ),
              child: Container(
                height: 550,
                color: Theme.of(context).primaryColor,
              ),
            ),
            ClipPath(
              clipper: ProsteBezierCurve(
                position: ClipPosition.bottom,
                list: [
                  BezierCurveSection(
                    start: Offset(0, 140),
                    top: Offset(size.width / 4, 170),
                    end: Offset(size.width / 2, 125),
                  ),
                  BezierCurveSection(
                    start: Offset(size.width / 2, 125),
                    top: Offset(size.width / 4 * 3, 120),
                    end: Offset(size.width, 170),
                  ),
                ],
              ),
              child: Container(
                height: 550,
                color: Theme.of(context).primaryColor.withOpacity(0.4),
              ),
            ),
            Form(
              key: _key,
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.25,
                      ),
                      Text(
                        "Login to Douleia",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 28),
                      ),
                      SizedBox(
                          height: MediaQuery.of(context).size.height * 0.03),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            "Select User Type",
                            style: TextStyle(
                                fontWeight: FontWeight.w600, fontSize: 18),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Row(children: [
                            Radio(
                              value: "employee",
                              groupValue: groupValue,
                              onChanged: (String? val) {
                                setState(() {
                                  groupValue = val!;
                                });
                              },
                            ),
                            Text("Employee"),
                          ]),
                          Spacer(),
                          Row(children: [
                            Radio(
                              value: "employer",
                              groupValue: groupValue,
                              onChanged: (String? val) {
                                setState(() {
                                  groupValue = val!;
                                });
                              },
                            ),
                            Text("Employer"),
                          ]),
                          SizedBox(width: 20),
                        ],
                      ),
                      SizedBox(height: 8),
                      TextFormField(
                        controller: _emailController,
                        validator: (String? val) => val!.isEmpty
                            ? "field can't be empty"
                            : !validate_email(val)
                                ? "enter correct email address"
                                : null,
                        decoration: InputDecoration(
                          label: Text("Enter Email *"),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      ),
                      SizedBox(height: 16),
                      TextFormField(
                        controller: _passwordController,
                        validator: (String? val) =>
                            val!.isEmpty ? "field can't be empty" : null,
                        obscureText: obscurePasswd,
                        decoration: InputDecoration(
                          label: Text("Enter Password *"),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          suffixIcon: IconButton(
                            onPressed: () =>
                                setState(() => obscurePasswd = !obscurePasswd),
                            icon: Icon(obscurePasswd
                                ? Icons.visibility
                                : Icons.visibility_off),
                          ),
                        ),
                      ),
                      SizedBox(
                          height: MediaQuery.of(context).size.height * 0.05),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: Theme.of(context).primaryColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            minimumSize: Size(double.maxFinite, 56)),
                        onPressed: () async {
                          if (_key.currentState!.validate() &&
                              groupValue != "userType") {
                            setState(() {
                              isLoading = true;
                            });

                            User? user = await AuthenticationService().signIn(
                                _emailController.text.trim(),
                                _passwordController.text.trim(),
                                context);
                            setState(() {
                              isLoading = false;
                            });
                            if (user != null && user.email!.isNotEmpty) {
                              if (groupValue ==
                                  context
                                      .read<AuthenticationProvider>()
                                      .userType) {
                                if (groupValue == 'employee') {
                                  Navigator.pushAndRemoveUntil(
                                    context,
                                    CupertinoPageRoute(
                                      builder: (context) =>
                                          EmployeeHomeScreen(),
                                    ),
                                    (route) => false,
                                  );
                                } else if (groupValue == 'employer') {
                                  Navigator.pushAndRemoveUntil(
                                    context,
                                    CupertinoPageRoute(
                                      builder: (context) =>
                                          EmployerHomeScreen(),
                                    ),
                                    (route) => false,
                                  );
                                }
                              }else{
                                toastMessage("select correct user type");
                              }
                            }
                          }
                        },
                        child: Text(
                          "Login",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 22),
                        ),
                      ),
                      SizedBox(
                          height: MediaQuery.of(context).size.height * 0.01),
                      Align(
                        alignment: Alignment.center,
                        child: RichText(
                          text: TextSpan(
                            text: "Don't have an Account?",
                            style: TextStyle(color: Colors.black),
                            children: [
                              TextSpan(
                                text: " Register",
                                style: TextStyle(color: Colors.red),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () => Navigator.push(
                                        context,
                                        CupertinoPageRoute(
                                            builder: (context) =>
                                                RegisterScreen()),
                                      ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                          height: MediaQuery.of(context).size.height * 0.02),
                      Text("-------- or sign in with ---------"),
                      SizedBox(height: 10),
                      GoogleSignInButton(
                          groupValue: groupValue, context: context),
                    ],
                  ),
                ),
              ),
            ),
            if (isLoading) loading()
          ],
        ),
      ),
    );
  }
}
