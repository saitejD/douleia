// import 'package:douleia/firebase_services/authentication_service.dart';
// import 'package:flutter/material.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:provider/provider.dart';

// class HomeScreen extends StatefulWidget {
//   const HomeScreen({Key? key}) : super(key: key);

//   @override
//   State<HomeScreen> createState() => _HomeScreenState();
// }

// class _HomeScreenState extends State<HomeScreen> {
//   @override
//   Widget build(BuildContext context) {
//     Size size = MediaQuery.of(context).size;
//     return Scaffold(
//       body: Container(
//         color: Color(0xFFF1F2F2),
//         height: double.infinity,
//         width: double.infinity,
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.start,
//           children: [
//             SizedBox(height: size.height*0.05),
//             Row(
//               mainAxisAlignment: MainAxisAlignment.end,
//               children: [
//                 Container(
//                   height: 50,
//                   width: 50,
//                   decoration: BoxDecoration(
//                     shape: BoxShape.circle,
//                     color: Colors.white,
//                   ),
//                   alignment: Alignment.center,
//                   child: FaIcon(
//                     Icons.search_outlined,
//                     size: 25,
//                     color: Colors.black,
//                   ),
//                 ),
//                 SizedBox(width: 10),
//                 Container(
//                   height: 50,
//                   width: 50,
//                   decoration: BoxDecoration(
//                     shape: BoxShape.circle,
//                     color: Colors.white,
//                   ),
//                   alignment: Alignment.center,
//                   child: IconButton(
//                     onPressed: () => context.read<AuthenticationService>().signOut(),
//                     icon: FaIcon(
//                       FontAwesomeIcons.signOut,
//                       size: 18,
//                       color: Colors.black,
//                     ),
//                   ),
//                 ),
//                 SizedBox(width: 30),
//               ],
//             )
//           ],
//         ),
//       ),
//     );
//   }
// }
