import 'dart:async';

import 'package:douleia/bloc/authentication_provider.dart';
import 'package:douleia/screens/register.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    var auth = Provider.of<AuthenticationProvider>(context,listen: false);
    auth.setCurrentUserType();
    super.initState();
  }
  @override
  void didChangeDependencies() {
    // context.read<AuthenticationProvider>().setCurrentUser();
    Timer(
      const Duration(seconds: 3),
      () => Navigator.pushReplacementNamed(context, '/auth'),
    );
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: [
          Image.asset(
            'assets/images/1.png',
            height: size.height,
            width: size.width,
            fit: BoxFit.cover,
            scale: 50.0,
          ),
          Positioned(
            top: size.height * 0.05,
            right: size.width * 0.1,
            child: Text(
              "Duoleia",
              style: TextStyle(
                  fontStyle: FontStyle.italic,
                  fontSize: 22,
                  fontWeight: FontWeight.w500),
            ),
          ),
          Positioned(
            top: size.height * 0.085,
            right: 0,
            child: Container(height: 2, width: 115, color: Colors.amberAccent),
          ),
          Positioned(
            top: size.height * 0.08,
            left: size.width * 0.05,
            child: Text(
              "Find your\ndream job\nhere",
              style: TextStyle(
                  fontSize: 32,
                  fontWeight: FontWeight.bold,
                  fontFamily: "PT Serif"),
            ),
          ),
          Positioned(
            top: size.height * 0.2,
            right: size.width * 0.12,
            child: Container(
              height: 60,
              width: 60,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.black.withOpacity(0.8),
              ),
              alignment: Alignment.center,
              child: FaIcon(
                FontAwesomeIcons.searchDollar,
                color: Colors.white,
              ),
            ),
          ),
          // Positioned(
          //   bottom: size.height * 0.02,
          //   right: size.width * 0.02,
          //   child: Container(
          //     height: 60,
          //     width: 60,
          //     decoration: BoxDecoration(
          //       shape: BoxShape.circle,
          //       color: Colors.black.withOpacity(0.8),
          //     ),
          //     alignment: Alignment.center,
          //     child: IconButton(
          //       onPressed: () {
          //         // Navigator.pushReplacement(
          //         //   context,
          //         //   CupertinoPageRoute(builder: (context) => RegisterScreen()),
          //         // );
          //       },
          //       icon: FaIcon(
          //         FontAwesomeIcons.arrowRight,
          //         color: Colors.white,
          //       ),
          //     ),
          //   ),
          // ),
        ],
      ),
    );
  }
}
