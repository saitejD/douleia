import 'package:douleia/firebase_services/database_service.dart';
import 'package:douleia/models/employee_model.dart';
import 'package:douleia/screens/employee/choose_category.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class EditEmployeeDetailsScreen extends StatefulWidget {
  final EmployeeModel empModel;
  const EditEmployeeDetailsScreen({Key? key, required this.empModel})
      : super(key: key);

  @override
  State<EditEmployeeDetailsScreen> createState() =>
      _EditEmployeeDetailsScreenState();
}

class _EditEmployeeDetailsScreenState extends State<EditEmployeeDetailsScreen> {
  late TextEditingController _nameController;
  late TextEditingController _dobController;
  late TextEditingController _previousWorkController;
  late TextEditingController _yearsOfExpController;
  late TextEditingController _expSalController;

  final GlobalKey<FormState> _globalKey = GlobalKey<FormState>();

  DateTime selectedDate = DateTime(2010);

  _selectDate(BuildContext context) async {
    final DateTime? selected = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1950),
        lastDate: DateTime(2010));
    if (selected != null && selected != selectedDate) {
      setState(() {
        // selectedDate = selected;
        _dobController.text = selected.toString();
      });
    }
  }

  Widget textField(TextEditingController controller, String title) {
    return TextFormField(
      controller: controller,
      validator: (String? val) => val!.isEmpty ? "field can't be empty" : null,
      decoration: InputDecoration(
        label: Text(title),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );
  }

  void setData() {
    _nameController = TextEditingController(text: widget.empModel.name);
    _dobController =
        TextEditingController(text: widget.empModel.dob.toString());

    _previousWorkController =
        TextEditingController(text: widget.empModel.previousJob);
    _yearsOfExpController =
        TextEditingController(text: widget.empModel.yearsOfExp);

    _expSalController =
        TextEditingController(text: widget.empModel.expectedSalary);
  }

  @override
  void initState() {
    setData();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.secondary,
      bottomNavigationBar: Container(
        padding: EdgeInsets.all(16),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Theme.of(context).primaryColor,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            minimumSize: Size(double.maxFinite, 56),
          ),
          onPressed: () async {
            if (_globalKey.currentState!.validate()) {
              // User? user = context.read<User?>();
              EmployeeModel model = EmployeeModel(
                userType: "employee",
                uid: FirebaseAuth.instance.currentUser!.uid,
                name: _nameController.text,
                dob: DateTime.parse(_dobController.text),
                previousJob: _previousWorkController.text,
                yearsOfExp: _yearsOfExpController.text,
                expectedSalary: _expSalController.text,
                interest: widget.empModel.interest,
                profilePic: widget.empModel.profilePic,
                bookMarks: widget.empModel.bookMarks,
                recentlyAppliedJobs: widget.empModel.recentlyAppliedJobs,
                id: widget.empModel.id,
              );
              DatabaseService().updateEmployee(widget.empModel.id, model);
              Navigator.pop(context);
            }
          },
          child: Text(
            "Edit",
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.w600, fontSize: 22),
          ),
        ),
      ),
      body: Container(
        height: size.height,
        width: size.width,
        color: Theme.of(context).colorScheme.secondary,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Form(
              key: _globalKey,
              child: Column(
                children: [
                  SizedBox(height: size.height * 0.05),
                  const Text(
                    "Edit your Details",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 28),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.03),
                  textField(_nameController, "Enter Your Name"),
                  SizedBox(height: 16),
                  TextFormField(
                    controller: _dobController,
                    readOnly: true,
                    onTap: () => _selectDate(context),
                    decoration: InputDecoration(
                      label: Text("Enter Your DOB"),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  SizedBox(height: 16),
                  textField(_previousWorkController, "Previous Work"),
                  SizedBox(height: 16),
                  textField(_yearsOfExpController, "Years of Experience"),
                  SizedBox(height: 16),
                  textField(_expSalController, "Expected Salary in Rupees"),
                  // SizedBox(height: size.height * 0.1),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
