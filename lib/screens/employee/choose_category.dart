import 'package:douleia/constants/categories.dart';
import 'package:douleia/firebase_services/database_service.dart';
import 'package:douleia/models/employee_model.dart';
import 'package:douleia/screens/employee/employee_home.dart';
import 'package:douleia/widgets/elevated_button.dart';
import 'package:douleia/widgets/loading_animation.dart';
import 'package:douleia/widgets/toastMessage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChooseCategoryScreen extends StatefulWidget {
  final EmployeeModel model;
  const ChooseCategoryScreen({Key? key, required this.model}) : super(key: key);

  @override
  State<ChooseCategoryScreen> createState() => _ChooseCategoryScreenState();
}

class _ChooseCategoryScreenState extends State<ChooseCategoryScreen> {
  int selectedIndex = -1;
  bool isLoading = false;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.secondary,
      body: Container(
        height: size.height,
        width: size.width,
        child: ListView(
          children: [
            SizedBox(height: size.height * 0.05),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Hi ${widget.model.name}!\nchoose one\nyou like",
                  style:
                      TextStyle(fontWeight: FontWeight.bold, fontSize: 32),
                ),
              ],
            ),
            SizedBox(height: size.height * 0.02),
            GridView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
              itemCount: categories.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                mainAxisSpacing: 0,
                crossAxisSpacing: 0,
              ),
              itemBuilder: (context, index) => Padding(
                // height: 100,
                // width: 100,
                // color: Colors.red,
                padding: EdgeInsets.all(8),
                child: Column(
                  children: [
                    Spacer(),
                    InkWell(
                      onTap: () => setState(() {
                        selectedIndex = index;
                      }),
                      child: Material(
                        elevation: 2,
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(40),
                        child: Stack(
                          children: [
                            Positioned(
                              top: 15,
                              left: 15,
                              child: Container(
                                height: 25,
                                width: 25,
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.red),
                                  color: selectedIndex == index
                                      ? Colors.red
                                      : Colors.white,
                                  shape: BoxShape.circle,
                                ),
                                child: selectedIndex == index
                                    ? Icon(
                                        Icons.done,
                                        size: 18,
                                        color: Colors.white,
                                      )
                                    : SizedBox.shrink(),
                              ),
                            ),
                            Container(
                              height: 130,
                              width: 130,
                              padding: EdgeInsets.all(36),
                              child: Image.asset(
                                categories[index]['asset'],
                                // height: 60,
                                // width: 60,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Spacer(),
                    Text(
                      categories[index]["name"],
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            // SizedBox(height: size.height * 0.1),
            // elevatedButton(
            //   context,
            //   "Continue",
            //   () async {
            //     if (selectedIndex != -1) {
            //       setState(() {
            //         isLoading = true;
            //       });
            //       widget.model.interest = categories[selectedIndex]['name'];
            //       await DatabaseService().addEmployee(widget.model);
            //       setState(() {
            //         isLoading = false;
            //       });
            //       Navigator.pushAndRemoveUntil(
            //         context,
            //         CupertinoPageRoute(
            //           builder: (context) => const EmployeeHomeScreen(),
            //         ),
            //         (predicate) => false,
            //       );
            //     } else
            //       toastMessage("select a category");
            //   },
            // ),
            // SizedBox(height: size.height * 0.02),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.all(16),
        // alignment: Alignment.bottomCenter,
        child: elevatedButton(
          context,
          "Continue",
          () async {
            if (selectedIndex != -1) {
              setState(() {
                isLoading = true;
              });
              widget.model.interest = categories[selectedIndex]['name'];
              await DatabaseService().addEmployee(widget.model);
              setState(() {
                isLoading = false;
              });
              Navigator.pushAndRemoveUntil(
                context,
                CupertinoPageRoute(
                  builder: (context) => const EmployeeHomeScreen(),
                ),
                (predicate) => false,
              );
            } else
              toastMessage("select a category");
          },
        ),
      ),
    );
  }
}
