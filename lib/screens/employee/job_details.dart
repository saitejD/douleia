import 'package:douleia/bloc/authentication_provider.dart';
import 'package:douleia/firebase_services/database_service.dart';
import 'package:douleia/models/job_model.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class JobDetailsScreen extends StatefulWidget {
  final JobModel jobModel;
  const JobDetailsScreen({Key? key, required this.jobModel}) : super(key: key);

  @override
  State<JobDetailsScreen> createState() => _JobDetailsScreenState();
}

class _JobDetailsScreenState extends State<JobDetailsScreen> {
  bool isLoading = false;
  Widget typeWidget(String title, String sub, IconData data, Color color) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        CircleAvatar(
          backgroundColor: color,
          radius: 25,
          child: Icon(
            data,
            color: Colors.black,
          ),
        ),
        SizedBox(height: 10),
        Text(
          title,
          style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w600,
              color: Colors.grey.shade600),
        ),
        Text(
          sub,
          style: TextStyle(
              fontSize: 14, fontWeight: FontWeight.bold, color: Colors.black),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.secondary,
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(16.0),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Theme.of(context).primaryColor,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
            minimumSize: Size(double.maxFinite, 56),
          ),
          onPressed: () async {
            setState(() {
              isLoading = true;
            });
            await DatabaseService().applyJob(
              widget.jobModel.id,
              context.read<AuthenticationProvider>().user!.uid,
            );
            setState(() {
              isLoading = false;
            });
            Navigator.pop(context);
          },
          child: Text(
            "Apply Now",
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.w600, fontSize: 22),
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            height: size.height,
            width: size.width,
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: size.height * 0.03),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      BackButton(
                        onPressed: () => Navigator.of(context).pop(),
                      ),
                      Spacer(),
                      Text(
                        widget.jobModel.companyName,
                        style: const TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                            fontStyle: FontStyle.italic),
                      ),
                      Spacer(),
                      IconButton(
                        onPressed: () async {
                          await DatabaseService().bookmarkJob(
                              context.read<AuthenticationProvider>().employeeModel!.id,
                              widget.jobModel.id);
                        },
                        icon: Icon(Icons.bookmark_border_rounded),
                      )
                    ],
                  ),
                  SizedBox(height: size.height * 0.03),
                  Container(
                    height: size.height * 0.25,
                    color: Colors.transparent,
                    child: Stack(
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: size.height * 0.07),
                          height: double.maxFinite,
                          decoration: BoxDecoration(
                              color: Colors.grey.shade300,
                              borderRadius: BorderRadius.circular(10)),
                          child: Column(
                            children: [
                              Spacer(),
                              Spacer(),
                              Spacer(),
                              Text(widget.jobModel.jobTitle,
                                  style: TextStyle(
                                      fontSize: 22,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black)),
                              Spacer(),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Spacer(),
                                  Text(
                                    widget.jobModel.companyName,
                                    style:
                                        TextStyle(fontWeight: FontWeight.w500),
                                  ),
                                  Spacer(),
                                  Text(
                                    "•",
                                    style: TextStyle(
                                        fontSize: 22,
                                        color: Colors.grey.shade500),
                                  ),
                                  Spacer(),
                                  Text(
                                    widget.jobModel.location,
                                    style:
                                        TextStyle(fontWeight: FontWeight.w500),
                                  ),
                                  Spacer(),
                                  Text(
                                    "•",
                                    style: TextStyle(
                                        fontSize: 22,
                                        color: Colors.grey.shade500),
                                  ),
                                  Spacer(),
                                  Text(
                                    DateFormat.yMMMd()
                                        .format(widget.jobModel.postedDate),
                                    style:
                                        TextStyle(fontWeight: FontWeight.w500),
                                  ),
                                  Spacer(),
                                ],
                              ),
                              Spacer()
                            ],
                          ),
                        ),
                        Align(
                            alignment: Alignment.topCenter,
                            child: CircleAvatar(
                              backgroundColor:
                                  Color.fromARGB(255, 183, 245, 160),
                              radius: 50,
                              child: Text(
                                widget.jobModel.companyName[0].toUpperCase(),
                                style: TextStyle(
                                    fontSize: 40,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black),
                              ),
                            ))
                      ],
                    ),
                  ),
                  SizedBox(height: size.height * 0.03),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(width: 20),
                      typeWidget(
                          "Salary",
                          "\$${widget.jobModel.salary}",
                          Icons.attach_money_outlined,
                          Color(0xffDCB26F).withOpacity(0.6)),
                      Spacer(),
                      typeWidget("Job Type", widget.jobModel.jobType,
                          Icons.work_outline_rounded, Color(0xffe2d3fe)),
                      Spacer(),
                      typeWidget("Position", widget.jobModel.position,
                          Icons.chair_outlined, Color(0xffbae4f4)),
                      SizedBox(width: 20),
                    ],
                  ),
                  SizedBox(height: size.height * 0.05),
                  Text(
                    "Description",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
                  ),
                  SizedBox(height: size.height * 0.02),
                  Text(widget.jobModel.description)
                ],
              ),
            ),
          ),
          if (isLoading)
            Center(
              child: CircularProgressIndicator(),
            )
        ],
      ),
    );
  }
}
