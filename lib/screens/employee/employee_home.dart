import 'package:douleia/bloc/authentication_provider.dart';
import 'package:douleia/firebase_services/authentication_service.dart';
import 'package:douleia/firebase_services/database_service.dart';
import 'package:douleia/models/employee_model.dart';
import 'package:douleia/models/job_model.dart';
import 'package:douleia/screens/employee/edit_employee.dart';
import 'package:douleia/screens/employee/job_details.dart';
import 'package:douleia/screens/employee/see_all_jobs.dart';
import 'package:douleia/screens/login.dart';
import 'package:douleia/widgets/error_message.dart';
import 'package:douleia/widgets/loading_animation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class EmployeeHomeScreen extends StatefulWidget {
  const EmployeeHomeScreen({Key? key}) : super(key: key);

  @override
  State<EmployeeHomeScreen> createState() => _EmployeeHomeScreenState();
}

class _EmployeeHomeScreenState extends State<EmployeeHomeScreen> {
  late Future<EmployeeModel> employee;

  Future<void> setU() async {
    employee = DatabaseService()
        .getEmployee(context.read<AuthenticationProvider>().currentUser.uid);
  }

  @override
  void initState() {
    setU();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // print("employee home page");
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.secondary,
      body: FutureBuilder<EmployeeModel>(
          future: employee,
          builder: (context, snapshots) {
            if (snapshots.connectionState == ConnectionState.waiting) {
              return loading();
            } else if (snapshots.hasError) {
              // print(snapshots.error);
              return errorMessage(size, "Something went wrong! Try Again");
            }
            if (snapshots.data == null) {
              return errorMessage(size, "Data not loaded");
            }
            return Body(
              employee: snapshots.data!,
            );
          }),
    );
  }
}

class Body extends StatefulWidget {
  EmployeeModel employee;
  Body({Key? key, required this.employee}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Container(
        color: Theme.of(context).colorScheme.secondary,
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: size.height * 0.05),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                // Container(
                //   height: 50,
                //   width: 50,
                //   decoration: BoxDecoration(
                //     shape: BoxShape.circle,
                //     color: Colors.white,
                //   ),
                //   alignment: Alignment.center,
                //   child: FaIcon(
                //     Icons.search_outlined,
                //     size: 25,
                //     color: Colors.black,
                //   ),
                // ),
                SizedBox(width: 10),
                Container(
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white,
                  ),
                  alignment: Alignment.center,
                  child: IconButton(
                    onPressed: () async {
                      await AuthenticationService().signOut();
                      Navigator.of(context).pushAndRemoveUntil(
                          CupertinoPageRoute(
                              builder: (context) => LoginScreen()),
                          (route) => false);
                    },
                    // context.read<AuthenticationService>().signOut(),
                    icon: FaIcon(
                      FontAwesomeIcons.signOut,
                      size: 18,
                      color: Colors.black,
                    ),
                  ),
                ),
                SizedBox(width: 10),
                Container(
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white,
                  ),
                  alignment: Alignment.center,
                  child: IconButton(
                    onPressed: () async {
                      await Navigator.push(
                        context,
                        CupertinoPageRoute(
                          builder: (context) => EditEmployeeDetailsScreen(
                              empModel: widget.employee),
                        ),
                      );
                      setState(() {});
                    },
                    // context.read<AuthenticationService>().signOut(),
                    icon: FaIcon(
                      Icons.person_outline_outlined,
                      // size: 18,
                      color: Colors.black,
                    ),
                  ),
                ),
                // SizedBox(width: 30),
              ],
            ),
            SizedBox(height: size.height * 0.01),
            Text(
              "Hello",
              style: TextStyle(
                  fontSize: 28,
                  fontWeight: FontWeight.w700,
                  color: Colors.grey),
            ),
            SizedBox(
              width: size.width * 0.7,
              child: Text(
                widget.employee.name + ".",
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: 28,
                  fontWeight: FontWeight.w700,
                  color: Colors.black,
                ),
              ),
            ),
            SizedBox(height: size.height * 0.02),
            JobsForYou(),
            SizedBox(height: size.height * 0.02),
            FindYourJob(),
            SizedBox(height: size.height * 0.02),
            RecentJobList(),
            SizedBox(height: size.height * 0.02),
          ],
        ),
      ),
    );
  }
}

class JobsForYou extends StatefulWidget {
  const JobsForYou({Key? key}) : super(key: key);

  @override
  State<JobsForYou> createState() => _JobsForYouState();
}

class _JobsForYouState extends State<JobsForYou> {
  late Future<List<JobModel>> jobs;

  Future<void> getJobs() async {
    // print(
    //     "cat ${context.read<AuthenticationProvider>().employeeModel!.interest}");
    jobs = DatabaseService().jobsForYou(
        context.read<AuthenticationProvider>().user!.uid,
        context.read<AuthenticationProvider>().employeeModel!.interest);
  }

  @override
  void initState() {
    getJobs();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return FutureBuilder<List<JobModel>>(
        future: jobs,
        builder: (context, snapshots) {
          if (snapshots.connectionState == ConnectionState.waiting) {
            return loadingList();
          } else if (snapshots.hasError) {
            // print(snapshots.error);
            return errorMessage(size, "Something went wrong! Try Again");
          }
          if (snapshots.data == null) {
            return errorMessage(size, "Data not loaded");
          }
          if (snapshots.data!.isEmpty) {
            return errorMessage(size, "No Data");
          }
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Jobs For You",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 22),
                  ),
                  GestureDetector(
                    onTap: () => Navigator.push(
                        context,
                        CupertinoPageRoute(
                            builder: (context) => SeeAllJobsScreen(
                                jobs: snapshots.data!, title: "Jobs For You"))),
                    child: Text(
                      "See All",
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w800,
                          color: Colors.black54),
                    ),
                  )
                ],
              ),
              SizedBox(height: size.height * 0.01),
              SizedBox(
                height: size.height * 0.3,
                child: ListView.builder(
                    itemCount: snapshots.data!.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      JobModel j = snapshots.data![index];
                      return GestureDetector(
                        onTap: () => Navigator.push(
                          context,
                          CupertinoPageRoute(
                              builder: (context) =>
                                  JobDetailsScreen(jobModel: j)),
                        ),
                        child: Container(
                          width: size.width * 0.4,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: Colors.white,
                          ),
                          margin:
                              EdgeInsets.only(right: 16, top: 10, bottom: 10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                height: 50,
                                width: 50,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 183, 245, 160),
                                    shape: BoxShape.circle),
                                child: Text(
                                  j.companyName[0].toUpperCase(),
                                  style: TextStyle(
                                      fontSize: 28,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              SizedBox(height: 5),
                              Text(
                                j.companyName,
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontWeight: FontWeight.w500),
                              ),
                              SizedBox(height: 5),
                              Text(
                                j.jobTitle,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(height: 20),
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    fixedSize: Size(130, 50),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10))),
                                onPressed: () {
                                  DatabaseService().applyJob(
                                    snapshots.data![index].id,
                                    context
                                        .read<AuthenticationProvider>()
                                        .user!
                                        .uid,
                                  );
                                },
                                child: Text("Apply Job"),
                              )
                            ],
                          ),
                        ),
                      );
                    }),
              )
            ],
          );
        });
  }
}

class FindYourJob extends StatefulWidget {
  @override
  State<FindYourJob> createState() => _FindYourJobState();
}

class _FindYourJobState extends State<FindYourJob> {
  late Future<List<List<JobModel>>> jobs;

  Future<void> getJobs() async {
    jobs = DatabaseService().typesOfJobs();
  }

  @override
  void initState() {
    getJobs();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return FutureBuilder<List<List<JobModel>>>(
        future: jobs,
        builder: (context, snapshots) {
          // print("here");
          // print(snapshots.data);
          if (snapshots.connectionState == ConnectionState.waiting) {
            return loadingList();
          } else if (snapshots.hasError) {
            // print(snapshots.error);
            return errorMessage(size, "Something went wrong! Try Again");
          }
          if (snapshots.data == null) {
            return errorMessage(size, "Data not loaded");
          }
          if (snapshots.data!.isEmpty) {
            return errorMessage(size, "No Data");
          }

          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                "Find Your Job",
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 22),
              ),
              SizedBox(height: size.height * 0.02),
              SizedBox(
                height: size.height * 0.3,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: () => Navigator.push(
                        context,
                        CupertinoPageRoute(
                          builder: (context) => SeeAllJobsScreen(
                              jobs: snapshots.data![0], title: "Remote Jobs"),
                        ),
                      ),
                      child: Container(
                        height: size.height * 0.3,
                        width: size.width * 0.43,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: Color(0xffbae4f4),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              height: 40,
                              width: 40,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  color: Colors.white, shape: BoxShape.circle),
                              child: FaIcon(Icons.home_outlined),
                            ),
                            SizedBox(height: 10),
                            Text(
                              snapshots.data![0].length.toString(),
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: 5),
                            Text(
                              "Remote Jobs",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.black38),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      width: size.width * 0.45,
                      height: size.height * 0.3,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          GestureDetector(
                            onTap: () => Navigator.push(
                              context,
                              CupertinoPageRoute(
                                builder: (context) => SeeAllJobsScreen(
                                    jobs: snapshots.data![1],
                                    title: "Fulltime Jobs"),
                              ),
                            ),
                            child: Container(
                              height: size.height * 0.14,
                              // width: double.maxFinite,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: Color(0xffe2d3fe),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    alignment: Alignment.center,
                                    height: 40,
                                    width: 40,
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        shape: BoxShape.circle),
                                    child: FaIcon(FontAwesomeIcons.building),
                                  ),
                                  SizedBox(width: 20),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        snapshots.data![1].length.toString(),
                                        style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(height: 5),
                                      Text(
                                        "Fulltime Jobs",
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w700,
                                            color: Colors.black38),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                          // SizedBox(height: 10),
                          GestureDetector(
                            onTap: () => Navigator.push(
                              context,
                              CupertinoPageRoute(
                                builder: (context) => SeeAllJobsScreen(
                                    jobs: snapshots.data![2],
                                    title: "Parttime Jobs"),
                              ),
                            ),
                            child: Container(
                              height: size.height * 0.14,
                              // width: size.width * 0.4,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: Color(0xffcdf1c0),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    height: 40,
                                    width: 40,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        shape: BoxShape.circle),
                                    child: FaIcon(Icons.favorite_border_sharp),
                                  ),
                                  SizedBox(width: 20),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        snapshots.data![2].length.toString(),
                                        style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(height: 5),
                                      Text(
                                        "Parttime Jobs",
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w700,
                                            color: Colors.black38),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          );
        });
  }
}

class RecentJobList extends StatefulWidget {
  const RecentJobList({Key? key}) : super(key: key);

  @override
  State<RecentJobList> createState() => _RecentJobListState();
}

class _RecentJobListState extends State<RecentJobList> {
  late Future<List<JobModel>> jobs;

  Future<void> getJobs() async {
    jobs = DatabaseService().recentlyApplidJobs(
        context.read<AuthenticationProvider>().employeeModel!.uid);
  }

  @override
  void initState() {
    getJobs();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return FutureBuilder<List<JobModel>>(
        future: jobs,
        builder: (context, snapshots) {
          if (snapshots.connectionState == ConnectionState.waiting) {
            return loadingList();
          } else if (snapshots.hasError) {
            // print(snapshots.error);
            return errorMessage(size, "Something went wrong! Try Again");
          }
          if (snapshots.data == null) {
            return errorMessage(size, "Data not loaded");
          }
          if (snapshots.data!.isEmpty) {
            return errorMessage(size, "No Data");
          }
          // print("here");
          // print(snapshots.data);
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Recent Job List",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 22),
                  ),
                  GestureDetector(
                    onTap: () => Navigator.push(
                      context,
                      CupertinoPageRoute(
                        builder: (context) => SeeAllJobsScreen(
                            jobs: snapshots.data!,
                            title: "Recently Applied Jobs"),
                      ),
                    ),
                    child: Text(
                      "See All",
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w800,
                          color: Colors.black54),
                    ),
                  )
                ],
              ),
              ListView.builder(
                itemCount: snapshots.data!.length,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  JobModel j = snapshots.data![index];
                  return GestureDetector(
                    onTap: () => Navigator.push(
                      context,
                      CupertinoPageRoute(
                        builder: (context) => JobDetailsScreen(jobModel: j),
                      ),
                    ),
                    child: Container(
                      margin: EdgeInsets.symmetric(vertical: 8),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10)),
                      padding:
                          EdgeInsets.symmetric(horizontal: 16, vertical: 24),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                height: 50,
                                width: 50,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 183, 245, 160),
                                    shape: BoxShape.circle),
                                child: Text(
                                  j.companyName[0].toUpperCase(),
                                  style: TextStyle(
                                      fontSize: 28,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              SizedBox(width: 10),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    j.jobTitle,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                        fontFamily: "PTSerif-Bold"),
                                  ),
                                  SizedBox(height: 5),
                                  Text(
                                    "\$${j.salary}",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        fontSize: 16,
                                        color: Colors.black54),
                                  )
                                ],
                              ),
                              Spacer(),
                              IconButton(
                                  onPressed: () async {
                                    await DatabaseService().bookmarkJob(
                                        context
                                            .read<AuthenticationProvider>()
                                            .employeeModel!
                                            .id,
                                        j.id);
                                  },
                                  icon: FaIcon(FontAwesomeIcons.bookmark)),
                            ],
                          ),
                          SizedBox(height: size.height * 0.02),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                constraints: BoxConstraints(
                                  minHeight: 40,
                                  minWidth: 130,
                                ),
                                padding: EdgeInsets.all(8),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: Theme.of(context)
                                        .colorScheme
                                        .secondary),
                                child: Text(
                                  j.position,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      color: Colors.black54),
                                ),
                              ),
                              Container(
                                constraints: BoxConstraints(
                                  minHeight: 40,
                                  minWidth: 100,
                                ),
                                padding: EdgeInsets.all(4),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: Theme.of(context)
                                        .colorScheme
                                        .secondary),
                                child: Text(
                                  j.jobType,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      color: Colors.black54),
                                ),
                              ),
                              GestureDetector(
                                onTap: () async {
                                  await DatabaseService().applyJob(
                                    j.id,
                                    context
                                        .read<AuthenticationProvider>()
                                        .user!
                                        .uid,
                                  );
                                  await getJobs();
                                },
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 20, vertical: 10),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(30),
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                  child: Text(
                                    "Apply",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        color: Colors.white),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                },
              )
            ],
          );
        });
  }
}
