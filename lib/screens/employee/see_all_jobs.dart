import 'package:douleia/bloc/authentication_provider.dart';
import 'package:douleia/firebase_services/database_service.dart';
import 'package:douleia/models/job_model.dart';
import 'package:douleia/screens/employee/job_details.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class SeeAllJobsScreen extends StatefulWidget {
  final List<JobModel> jobs;
  final String title;
  const SeeAllJobsScreen({Key? key, required this.jobs, required this.title})
      : super(key: key);

  @override
  State<SeeAllJobsScreen> createState() => _SeeAllJobsScreenState();
}

class _SeeAllJobsScreenState extends State<SeeAllJobsScreen> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.secondary,
      appBar: AppBar(title: Text(widget.title)),
      body: Container(
        padding: EdgeInsets.all(8),
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: ListView.builder(
          itemCount: widget.jobs.length,
          itemBuilder: (context, index) {
            JobModel j = widget.jobs[index];
            return GestureDetector(
              onTap: () => Navigator.push(
                context,
                CupertinoPageRoute(
                  builder: (context) => JobDetailsScreen(jobModel: j),
                ),
              ),
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 8),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10)),
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 24),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          height: 50,
                          width: 50,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: Color.fromARGB(255, 183, 245, 160),
                              shape: BoxShape.circle),
                          child: Text(
                            j.companyName[0].toUpperCase(),
                            style: TextStyle(
                                fontSize: 28, fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(width: 10),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              j.jobTitle,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  fontFamily: "PTSerif-Bold"),
                            ),
                            SizedBox(height: 5),
                            Text(
                              "\$${j.salary}",
                              style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 16,
                                  color: Colors.black54),
                            )
                          ],
                        ),
                        Spacer(),
                        IconButton(
                            onPressed: () async {
                              await DatabaseService().bookmarkJob(
                                  context
                                      .read<AuthenticationProvider>()
                                      .employeeModel!
                                      .id,
                                  j.id);
                            },
                            icon: FaIcon(FontAwesomeIcons.bookmark)),
                      ],
                    ),
                    SizedBox(height: size.height * 0.02),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          constraints: BoxConstraints(
                            minHeight: 40,
                            minWidth: 130,
                          ),
                          padding: EdgeInsets.all(8),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              color: Theme.of(context).colorScheme.secondary),
                          child: Text(
                            j.position,
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                color: Colors.black54),
                          ),
                        ),
                        Container(
                          constraints: BoxConstraints(
                            minHeight: 40,
                            minWidth: 100,
                          ),
                          padding: EdgeInsets.all(4),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              color: Theme.of(context).colorScheme.secondary),
                          child: Text(
                            j.jobType,
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                color: Colors.black54),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 10),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              color: Theme.of(context).colorScheme.primary),
                          child: Text(
                            "Apply",
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                color: Colors.white),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
