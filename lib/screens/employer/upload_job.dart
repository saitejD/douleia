import 'package:douleia/firebase_services/database_service.dart';
import 'package:douleia/models/job_model.dart';
import 'package:douleia/widgets/loading_animation.dart';
import 'package:flutter/material.dart';

class UploadJobScreen extends StatefulWidget {
  final String companyName;
  final String companyUid;
  const UploadJobScreen(
      {Key? key, required this.companyName, required this.companyUid})
      : super(key: key);

  @override
  State<UploadJobScreen> createState() => _UploadJobScreenState();
}

class _UploadJobScreenState extends State<UploadJobScreen> {
  // TextEditingController _nameController = TextEditingController();
  TextEditingController _jobTitleController = TextEditingController();
  TextEditingController _locationController = TextEditingController();
  TextEditingController _salaryontroller = TextEditingController();
  TextEditingController _jobTypenController = TextEditingController();
  TextEditingController _positionontroller = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();
  TextEditingController _jobCategoryController = TextEditingController();

  final GlobalKey<FormState> _globalKey = GlobalKey<FormState>();

  DateTime selectedDate = DateTime(2010);

  bool isLoading = false;

  Widget textField(TextEditingController controller, String title) {
    return TextFormField(
      controller: controller,
      validator: (String? val) => val!.isEmpty ? "field can't be empty" : null,
      decoration: InputDecoration(
        label: Text(title),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: size.height,
            width: size.width,
            color: Theme.of(context).colorScheme.secondary,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Form(
                  key: _globalKey,
                  child: Column(
                    children: [
                      SizedBox(height: size.height * 0.05),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          BackButton(),
                          Spacer(),
                          Text(
                            "Add Job Details",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 28),
                          ),
                          Spacer(),
                        ],
                      ),
                      SizedBox(height: 16),
                      textField(_jobTitleController, "Job Title"),
                      SizedBox(height: 16),
                      textField(_jobCategoryController, "Job Category"),
                      SizedBox(height: 16),
                      textField(_locationController, "Job Location/City"),
                      SizedBox(height: 16),
                      // textField(
                      //     _locationController, "Company Headquarters Location"),
                      // SizedBox(height: 16),
                      textField(_salaryontroller, "Estimated Salary"),
                      SizedBox(height: 16),
                      textField(
                          _jobTypenController, "Type of Job Parttime/Fulltime"),
                      SizedBox(height: 16),
                      textField(_positionontroller, "Position of Job role"),
                      SizedBox(height: 16),
                      textField(
                          _descriptionController, "Describe about the Job"),
                      // SizedBox(height: size.height * 0.1),
                    ],
                  ),
                ),
              ),
            ),
          ),
          if (isLoading) loading()
        ],
      ),
      backgroundColor: Theme.of(context).colorScheme.secondary,
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(16.0),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Theme.of(context).primaryColor,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            minimumSize: Size(double.maxFinite, 56),
          ),
          onPressed: () async {
            if (_globalKey.currentState!.validate()) {
              setState(() {
                isLoading = true;
              });
              JobModel model = JobModel(
                companyName: widget.companyName,
                jobTitle: _jobTitleController.text,
                location: _locationController.text,
                postedDate: DateTime.now(),
                salary: _salaryontroller.text,
                jobType: _jobTypenController.text,
                position: _positionontroller.text,
                description: _descriptionController.text,
                companyUid: widget.companyUid,
                jobCategory: _jobCategoryController.text,
                appliedPeople: [],
                id: "",
              );
              await DatabaseService().addJob(model);
              setState(() {
                isLoading = false;
              });
              Navigator.pop(context);
            }
          },
          child: Text(
            "Continue",
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.w600, fontSize: 22),
          ),
        ),
      ),
    );
  }
}
