import 'package:douleia/models/employee_model.dart';
import 'package:douleia/screens/employer/applicant_detail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SeePeopleScreen extends StatefulWidget {
  final List<EmployeeModel> emp;
  const SeePeopleScreen({Key? key, required this.emp}) : super(key: key);

  @override
  State<SeePeopleScreen> createState() => _SeePeopleScreenState();
}

class _SeePeopleScreenState extends State<SeePeopleScreen> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text("Applied People"),
      ),
      body: ListView.builder(
        itemCount: widget.emp.length,
        itemBuilder: (context, index) {
          EmployeeModel e = widget.emp[index];
          return GestureDetector(
            onTap: () => Navigator.push(
                context,
                CupertinoPageRoute(
                    builder: (context) =>
                        ApplicantDetailsScreen(employeeModel: e))),
            child: Container(
              width: size.width * 0.4,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: Colors.white,
              ),
              margin: EdgeInsets.only(right: 16, top: 10, bottom: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 50,
                    width: 50,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        color: Color.fromARGB(255, 183, 245, 160),
                        shape: BoxShape.circle),
                    child: Text(
                      e.name[0].toUpperCase(),
                      style:
                          TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(height: 5),
                  Text(
                    e.previousJob,
                    style: TextStyle(
                        color: Colors.black87, fontWeight: FontWeight.w500),
                  ),
                  SizedBox(height: 5),
                  Text(
                    e.name,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 20),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        fixedSize: Size(130, 50),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10))),
                    onPressed: () {},
                    child: Text("Respond"),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
