import 'package:douleia/firebase_services/database_service.dart';
import 'package:douleia/models/job_model.dart';
import 'package:douleia/widgets/loading_animation.dart';
import 'package:flutter/material.dart';

class EditJobScreen extends StatefulWidget {
  final JobModel jobModel;
  const EditJobScreen({Key? key, required this.jobModel}) : super(key: key);

  @override
  State<EditJobScreen> createState() => _EditJobScreenState();
}

class _EditJobScreenState extends State<EditJobScreen> {
  // TextEditingController _nameController = TextEditingController();
  TextEditingController _jobTitleController = TextEditingController();
  TextEditingController _locationController = TextEditingController();
  TextEditingController _salaryontroller = TextEditingController();
  TextEditingController _jobTypenController = TextEditingController();
  TextEditingController _positionontroller = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();
  TextEditingController _jobCategoryController = TextEditingController();

  final GlobalKey<FormState> _globalKey = GlobalKey<FormState>();

  DateTime selectedDate = DateTime(2010);

  bool isLoading = false;

  Widget textField(TextEditingController controller, String title) {
    return TextFormField(
      controller: controller,
      validator: (String? val) => val!.isEmpty ? "field can't be empty" : null,
      decoration: InputDecoration(
        label: Text(title),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );
  }

  void setData() {
    _jobTitleController = TextEditingController(text: widget.jobModel.jobTitle);
    _locationController = TextEditingController(text: widget.jobModel.location);

    _salaryontroller = TextEditingController(text: widget.jobModel.salary);
    _jobTypenController = TextEditingController(text: widget.jobModel.jobType);

    _positionontroller = TextEditingController(text: widget.jobModel.position);
    _descriptionController =
        TextEditingController(text: widget.jobModel.description);
    _jobCategoryController =
        TextEditingController(text: widget.jobModel.jobCategory);
  }

  @override
  void initState() {
    // TODO: implement initState
    setData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: size.height,
            width: size.width,
            color: Theme.of(context).colorScheme.secondary,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Form(
                  key: _globalKey,
                  child: Column(
                    children: [
                      SizedBox(height: size.height * 0.05),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          BackButton(),
                          Spacer(),
                          Text(
                            "Edit Job Details",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 28),
                          ),
                          Spacer(),
                        ],
                      ),
                      SizedBox(height: 16),
                      textField(_jobTitleController, "Job Title"),
                      SizedBox(height: 16),
                      textField(_jobCategoryController, "Job Category"),
                      SizedBox(height: 16),
                      textField(_locationController, "Job Location/City"),
                      SizedBox(height: 16),
                      textField(_salaryontroller, "Estimated Salary"),
                      SizedBox(height: 16),
                      textField(
                          _jobTypenController, "Type of Job Parttime/Fulltime"),
                      SizedBox(height: 16),
                      textField(_positionontroller, "Position of Job role"),
                      SizedBox(height: 16),
                      textField(
                          _descriptionController, "Describe about the Job"),
                    ],
                  ),
                ),
              ),
            ),
          ),
          if (isLoading) loading()
        ],
      ),
      backgroundColor: Theme.of(context).colorScheme.secondary,
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(16.0),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Theme.of(context).primaryColor,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            minimumSize: Size(double.maxFinite, 56),
          ),
          onPressed: () async {
            if (_globalKey.currentState!.validate()) {
              setState(() {
                isLoading = true;
              });
              JobModel model = JobModel(
                companyName: widget.jobModel.companyName,
                jobTitle: _jobTitleController.text,
                location: _locationController.text,
                postedDate: widget.jobModel.postedDate,
                salary: _salaryontroller.text,
                jobType: _jobTypenController.text,
                position: _positionontroller.text,
                description: _descriptionController.text,
                companyUid: widget.jobModel.companyUid,
                jobCategory: _jobCategoryController.text,
                appliedPeople: widget.jobModel.appliedPeople,
                id: widget.jobModel.id,
              );
              await DatabaseService().updateJob(widget.jobModel.id, model);
              setState(() {
                isLoading = false;
              });
              Navigator.pop(context);
            }
          },
          child: Text(
            "Update",
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.w600, fontSize: 22),
          ),
        ),
      ),
    );
  }
}
