import 'package:douleia/bloc/authentication_provider.dart';
import 'package:douleia/firebase_services/authentication_service.dart';
import 'package:douleia/firebase_services/database_service.dart';
import 'package:douleia/models/employee_model.dart';
import 'package:douleia/models/employer_model.dart';
import 'package:douleia/models/job_model.dart';
import 'package:douleia/screens/employee/job_details.dart';
import 'package:douleia/screens/employee/see_all_jobs.dart';
import 'package:douleia/screens/employer/applicant_detail.dart';
import 'package:douleia/screens/employer/edit_company.dart';
import 'package:douleia/screens/employer/edit_job.dart';
import 'package:douleia/screens/employer/see_people.dart';
import 'package:douleia/screens/employer/upload_job.dart';
import 'package:douleia/screens/login.dart';
import 'package:douleia/screens/register.dart';
import 'package:douleia/widgets/error_message.dart';
import 'package:douleia/widgets/loading_animation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class EmployerHomeScreen extends StatefulWidget {
  const EmployerHomeScreen({Key? key}) : super(key: key);

  @override
  State<EmployerHomeScreen> createState() => _EmployerHomeScreenState();
}

class _EmployerHomeScreenState extends State<EmployerHomeScreen> {
  late Future<EmployerModel> employer;

  Future<void> setU() async {
    employer = DatabaseService()
        .getEmployer(context.read<AuthenticationProvider>().currentUser.uid);
  }

  // @override
  // void didChangeDependencies() async {
  //   await setU();
  //   super.didChangeDependencies();
  // }

  @override
  void initState() {
    setU();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print("employer home screen");
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: FutureBuilder<EmployerModel>(
        future: employer,
        builder: (context, snapshots) {
          if (snapshots.connectionState == ConnectionState.waiting) {
            return loading();
          } else if (snapshots.hasError) {
            return errorMessage(size, "Something went wrong! Try Again");
          }
          if (snapshots.data == null) {
            return errorMessage(size, "Data not loaded");
          }
          return Body(employer: snapshots.data!);
        },
      ),
    );
  }
}

class Body extends StatefulWidget {
  EmployerModel employer;
  Body({Key? key, required this.employer}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Stack(
      children: [
        SingleChildScrollView(
          child: Container(
            color: Theme.of(context).colorScheme.secondary,
            padding: EdgeInsets.symmetric(horizontal: 16),
            // height: size.height,
            // width: size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: size.height * 0.05),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      height: 50,
                      width: 50,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white,
                      ),
                      alignment: Alignment.center,
                      child: IconButton(
                        onPressed: () async {
                          await AuthenticationService().signOut();
                          Navigator.pushAndRemoveUntil(
                              context,
                              CupertinoPageRoute(
                                  builder: (context) => LoginScreen()),
                              (route) => false);
                        },
                        icon: FaIcon(
                          FontAwesomeIcons.signOut,
                          size: 18,
                          color: Colors.black,
                        ),
                      ),
                    ),
                    SizedBox(width: 10),
                    GestureDetector(
                      onTap: () async {
                        await Navigator.push(
                          context,
                          CupertinoPageRoute(
                              builder: (context) => EditCompanyDetailsScreen(
                                    employerModel: context
                                        .read<AuthenticationProvider>()
                                        .employerModel!,
                                  )),
                        );
                        setState(() {});
                      },
                      child: Container(
                        height: 50,
                        width: 50,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.white,
                        ),
                        alignment: Alignment.center,
                        child: FaIcon(
                          Icons.person_outline_rounded,
                          size: 25,
                          color: Colors.black,
                        ),
                      ),
                    ),
                    SizedBox(width: 10),
                  ],
                ),
                SizedBox(height: size.height * 0.02),
                Text(
                  "Hello",
                  style: TextStyle(
                      fontSize: 28,
                      fontWeight: FontWeight.w700,
                      color: Colors.grey),
                ),
                SizedBox(
                  width: size.width * 0.7,
                  child: Text(
                    widget.employer.companyName + ".",
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 28,
                      fontWeight: FontWeight.w700,
                      color: Colors.black,
                    ),
                  ),
                ),
                SizedBox(height: size.height * 0.02),
                PeopleApplied(),
                SizedBox(height: size.height * 0.02),
                // FindYourJob(),
                // SizedBox(height: size.height * 0.02),
                RecentJobsPosted(),
                SizedBox(height: size.height * 0.15),
              ],
            ),
          ),
        ),
        Positioned(
          bottom: 0,
          child: Stack(
            children: [
              Container(
                height: 90,
                width: size.width,
                color: Colors.transparent,
                padding: EdgeInsets.only(top: 32),
                child: Container(
                  color: Theme.of(context).colorScheme.secondary,
                ),
              ),
              Positioned(
                  left: size.width * 0.4,
                  bottom: 5,
                  child: InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        CupertinoPageRoute(
                            builder: (context) => UploadJobScreen(
                                  companyUid: widget.employer.uid,
                                  companyName: widget.employer.companyName,
                                )),
                      );
                    },
                    child: Container(
                      height: 80,
                      width: 80,
                      decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(25)),
                      child: Icon(
                        Icons.add_outlined,
                        size: 40,
                        color: Colors.white,
                      ),
                    ),
                  ))
            ],
          ),
        )
      ],
    );
  }
}

class PeopleApplied extends StatefulWidget {
  @override
  State<PeopleApplied> createState() => _PeopleAppliedState();
}

class _PeopleAppliedState extends State<PeopleApplied> {
  late Future<List<EmployeeModel>> emp;

  void getEmp() {
    emp = DatabaseService()
        .peopleApplied(context.read<AuthenticationProvider>().user!.uid);
  }

  @override
  void initState() {
    getEmp();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return FutureBuilder<List<EmployeeModel>>(
        future: emp,
        builder: (context, snapshots) {
          if (snapshots.connectionState == ConnectionState.waiting) {
            return loadingList();
          } else if (snapshots.hasError) {
            // print(snapshots.error);
            return errorMessage(size, "Something went wrong! Try Again");
          }
          if (snapshots.data == null) {
            return errorMessage(size, "Data not loaded");
          }
          if (snapshots.data!.isEmpty) {
            return errorMessage(size, "No Data");
          }
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Applied People",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 22),
                  ),
                  GestureDetector(
                    onTap: () => Navigator.push(
                        context,
                        CupertinoPageRoute(
                            builder: (context) =>
                                SeePeopleScreen(emp: snapshots.data!))),
                    child: Text(
                      "See All",
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w800,
                          color: Colors.black54),
                    ),
                  )
                ],
              ),
              SizedBox(height: size.height * 0.01),
              SizedBox(
                height: size.height * 0.3,
                child: ListView.builder(
                    itemCount: snapshots.data!.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      EmployeeModel e = snapshots.data![index];
                      return GestureDetector(
                        onTap: () => Navigator.push(
                            context,
                            CupertinoPageRoute(
                                builder: (context) =>
                                    ApplicantDetailsScreen(employeeModel: e))),
                        child: Container(
                          width: size.width * 0.4,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: Colors.white,
                          ),
                          margin:
                              EdgeInsets.only(right: 16, top: 10, bottom: 10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                height: 50,
                                width: 50,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 183, 245, 160),
                                    shape: BoxShape.circle),
                                child: Text(
                                  e.name[0].toUpperCase(),
                                  style: TextStyle(
                                      fontSize: 28,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              SizedBox(height: 5),
                              Text(
                                e.previousJob,
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontWeight: FontWeight.w500),
                              ),
                              SizedBox(height: 5),
                              Text(
                                e.name,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(height: 20),
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    fixedSize: Size(130, 50),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10))),
                                onPressed: () {},
                                child: Text("Respond"),
                              )
                            ],
                          ),
                        ),
                      );
                    }),
              )
            ],
          );
        });
  }
}

class FindYourJob extends StatelessWidget {
  const FindYourJob({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Text(
          "Find Your Job",
          style: TextStyle(fontWeight: FontWeight.w600, fontSize: 22),
        ),
        SizedBox(height: size.height * 0.02),
        SizedBox(
          height: size.height * 0.3,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: size.height * 0.3,
                width: size.width * 0.43,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Color(0xffbae4f4),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: 40,
                      width: 40,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: Colors.white, shape: BoxShape.circle),
                      child: FaIcon(Icons.home_outlined),
                    ),
                    SizedBox(height: 10),
                    Text(
                      "44.8k",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 5),
                    Text(
                      "Remote Jobs",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          color: Colors.black38),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: size.width * 0.45,
                height: size.height * 0.3,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: size.height * 0.14,
                      // width: double.maxFinite,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Color(0xffe2d3fe),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            alignment: Alignment.center,
                            height: 40,
                            width: 40,
                            decoration: BoxDecoration(
                                color: Colors.white, shape: BoxShape.circle),
                            child: FaIcon(FontAwesomeIcons.building),
                          ),
                          SizedBox(width: 20),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "21.8k",
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 5),
                              Text(
                                "Fulltime Jobs",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    color: Colors.black38),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    // SizedBox(height: 10),
                    Container(
                      height: size.height * 0.14,
                      // width: size.width * 0.4,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Color(0xffcdf1c0),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            height: 40,
                            width: 40,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: Colors.white, shape: BoxShape.circle),
                            child: FaIcon(Icons.favorite_border_sharp),
                          ),
                          SizedBox(width: 20),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "17k",
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 5),
                              Text(
                                "Parttime Jobs",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    color: Colors.black38),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}

class RecentJobsPosted extends StatefulWidget {
  const RecentJobsPosted({Key? key}) : super(key: key);

  @override
  State<RecentJobsPosted> createState() => _RecentJobsPostedState();
}

class _RecentJobsPostedState extends State<RecentJobsPosted> {
  late Future<List<JobModel>> jobs;

  void getEmp() {
    jobs = DatabaseService()
        .recentlyPostedJobs(context.read<AuthenticationProvider>().user!.uid);
  }

  @override
  void initState() {
    getEmp();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return FutureBuilder<List<JobModel>>(
        future: jobs,
        builder: (context, snapshots) {
          if (snapshots.connectionState == ConnectionState.waiting) {
            return loading();
          } else if (snapshots.hasError) {
            // print(snapshots.error);
            return errorMessage(size, "Something went wrong! Try Again");
          }
          if (snapshots.data == null) {
            return errorMessage(size, "Data not loaded");
          }
          if (snapshots.data!.isEmpty) {
            return errorMessage(size, "No Data");
          }
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Recent Jobs Posted",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 22),
                  ),
                  GestureDetector(
                    onTap: () => Navigator.push(
                        context,
                        CupertinoPageRoute(
                            builder: (context) => SeeAllJobsScreen(
                                jobs: snapshots.data!,
                                title: "Recent Jobs Posted"))),
                    child: Text(
                      "See All",
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w800,
                          color: Colors.black54),
                    ),
                  )
                ],
              ),
              ListView.builder(
                itemCount: snapshots.data!.length,
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  JobModel j = snapshots.data![index];
                  return GestureDetector(
                    onTap: () => Navigator.push(
                        context,
                        CupertinoPageRoute(
                            builder: (cotnxx) =>
                                JobDetailsScreen(jobModel: j))),
                    child: Container(
                      margin: EdgeInsets.symmetric(vertical: 8),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10)),
                      padding:
                          EdgeInsets.symmetric(horizontal: 16, vertical: 24),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                height: 50,
                                width: 50,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 183, 245, 160),
                                    shape: BoxShape.circle),
                                child: Text(
                                  j.companyName[0].toUpperCase(),
                                  style: TextStyle(
                                      fontSize: 28,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              SizedBox(width: 10),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    j.jobTitle,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                        fontFamily: "PTSerif-Bold"),
                                  ),
                                  SizedBox(height: 5),
                                  Text(
                                    "\$ ${j.salary}",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        fontSize: 16,
                                        color: Colors.black54),
                                  )
                                ],
                              ),
                              Spacer(),
                              IconButton(
                                onPressed: () async {
                                  await DatabaseService().deleteJob(j.id);
                                  setState(() {
                                    getEmp();
                                  });
                                },
                                icon: FaIcon(FontAwesomeIcons.trash),
                              ),
                            ],
                          ),
                          SizedBox(height: size.height * 0.02),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                constraints: BoxConstraints(
                                  minHeight: 40,
                                  minWidth: 130,
                                ),
                                padding: EdgeInsets.all(8),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: Theme.of(context)
                                        .colorScheme
                                        .secondary),
                                child: Text(
                                  j.position,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      color: Colors.black54),
                                ),
                              ),
                              Container(
                                constraints: BoxConstraints(
                                  minHeight: 40,
                                  minWidth: 100,
                                ),
                                padding: EdgeInsets.all(4),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: Theme.of(context)
                                        .colorScheme
                                        .secondary),
                                child: Text(
                                  j.jobType,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      color: Colors.black54),
                                ),
                              ),
                              GestureDetector(
                                onTap: () async {
                                  await Navigator.push(
                                      context,
                                      CupertinoPageRoute(
                                          builder: (context) =>
                                              EditJobScreen(jobModel: j)));
                                  setState(() {
                                    getEmp();
                                  });
                                },
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 20, vertical: 10),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(30),
                                      color: Theme.of(context)
                                          .colorScheme
                                          .primary),
                                  child: Text(
                                    "Modify",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        color: Colors.white),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                },
              )
            ],
          );
        });
  }
}
