import 'package:douleia/bloc/authentication_provider.dart';
import 'package:douleia/firebase_services/database_service.dart';
import 'package:douleia/models/employee_model.dart';
import 'package:douleia/models/employer_model.dart';
import 'package:douleia/screens/employee/choose_category.dart';
import 'package:douleia/screens/employer/employer_home.dart';
import 'package:douleia/widgets/loading_animation.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class EmployerDetailsScreen extends StatefulWidget {
  final String dp;
  const EmployerDetailsScreen({Key? key, required this.dp}) : super(key: key);

  @override
  State<EmployerDetailsScreen> createState() => _EmployerDetailsScreenState();
}

class _EmployerDetailsScreenState extends State<EmployerDetailsScreen> {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _startedYearController = TextEditingController();
  TextEditingController _locationController = TextEditingController();
  TextEditingController _noOfEmployeesController = TextEditingController();
  TextEditingController _companyDescriptionController = TextEditingController();

  final GlobalKey<FormState> _globalKey = GlobalKey<FormState>();

  DateTime selectedDate = DateTime(2010);

  bool isLoading = false;

  _selectDate(BuildContext context) async {
    final DateTime? selected = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1950),
        lastDate: DateTime(2010));
    if (selected != null && selected != selectedDate) {
      setState(() {
        // selectedDate = selected;
        _startedYearController.text = selected.toString();
      });
    }
  }

  Widget textField(TextEditingController controller, String title) {
    return TextFormField(
      controller: controller,
      validator: (String? val) => val!.isEmpty ? "field can't be empty" : null,
      decoration: InputDecoration(
        label: Text(title),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: size.height,
            width: size.width,
            color: Theme.of(context).colorScheme.secondary,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Form(
                  key: _globalKey,
                  child: Column(
                    children: [
                      SizedBox(height: size.height * 0.05),
                      Text(
                        "Add your company Details",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 28),
                      ),
                      SizedBox(
                          height: MediaQuery.of(context).size.height * 0.03),
                      textField(_nameController, "Your Company Name"),
                      SizedBox(height: 16),
                      // textField(
                      //   _dobController, "Enter Your DOB"),
                      TextFormField(
                        controller: _startedYearController,
                        readOnly: true,
                        onTap: () => _selectDate(context),
                        decoration: InputDecoration(
                          label: Text("Company Started Year"),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      ),
                      SizedBox(height: 16),
                      textField(
                          _locationController, "Company Headquarters Location"),
                      SizedBox(height: 16),
                      textField(
                          _noOfEmployeesController, "No.Of Employees Working"),
                      SizedBox(height: 16),
                      textField(_companyDescriptionController,
                          "Describe Your Compnay"),
                      // SizedBox(height: size.height * 0.1),
                    ],
                  ),
                ),
              ),
            ),
          ),
          if (isLoading) loading()
        ],
      ),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.all(16),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Theme.of(context).primaryColor,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            minimumSize: Size(double.maxFinite, 56),
          ),
          onPressed: () async {
            if (_globalKey.currentState!.validate()) {
              setState(() {
                isLoading = true;
              });
              EmployerModel model = EmployerModel(
                uid: FirebaseAuth.instance.currentUser!.uid,
                userType: "employer",
                companyName: _nameController.text,
                startedYear: DateTime.parse(_startedYearController.text),
                noofEmployees: _noOfEmployeesController.text,
                companyLocation: _locationController.text,
                companyDescription: _companyDescriptionController.text,
                profilePic: widget.dp,
                appliedPeople: [],
                addedJobs: [],
                id: "",
              );
              await DatabaseService().addEmployer(model);
              // context.read<AuthenticationProvider>().setCurrentUser();
              Future.delayed(Duration(seconds: 2));
              setState(() {
                isLoading = false;
              });
              Navigator.pushAndRemoveUntil(
                context,
                CupertinoPageRoute(
                  builder: (context) => const EmployerHomeScreen(),
                ),
                (predict) => false,
              );
            }
          },
          child: Text(
            "Continue",
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.w600, fontSize: 22),
          ),
        ),
      ),
    );
  }
}
