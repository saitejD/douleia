import 'package:douleia/models/employee_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:intl/intl.dart';

class ApplicantDetailsScreen extends StatefulWidget {
  final EmployeeModel employeeModel;
  const ApplicantDetailsScreen({Key? key, required this.employeeModel})
      : super(key: key);

  @override
  State<ApplicantDetailsScreen> createState() => _ApplicantDetailsScreenState();
}

class _ApplicantDetailsScreenState extends State<ApplicantDetailsScreen> {
  bool isLoading = false;
  Widget typeWidget(String title, String sub, IconData data, Color color) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        CircleAvatar(
          backgroundColor: color,
          radius: 25,
          child: Icon(
            data,
            color: Colors.black,
          ),
        ),
        SizedBox(height: 10),
        Text(
          title,
          style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w600,
              color: Colors.grey.shade600),
        ),
        Text(
          sub,
          style: TextStyle(
              fontSize: 14, fontWeight: FontWeight.bold, color: Colors.black),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.secondary,
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(16.0),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Theme.of(context).primaryColor,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
            minimumSize: Size(double.maxFinite, 56),
          ),
          onPressed: () async {
            setState(() {
              isLoading = true;
            });
            final Email email = Email(
              body: 'We have seen your details and approach to our job. Will you be available for a meeting tomorrow.',
              subject: 'Regarding Job Application',
              recipients: ['iamhere@gmail.com'],
              // cc: ['cc@example.com'],
              // bcc: ['bcc@example.com'],
              // attachmentPaths: ['/path/to/attachment.zip'],
              isHTML: false,
            );

            await FlutterEmailSender.send(email);
            setState(() {
              isLoading = false;
            });
            Navigator.pop(context);
          },
          child: Text(
            "Respond",
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.w600, fontSize: 22),
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            height: size.height,
            width: size.width,
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: size.height * 0.03),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      BackButton(
                        onPressed: () => Navigator.of(context).pop(),
                      ),
                      Spacer(),
                      Text(
                        widget.employeeModel.name,
                        style: const TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                            fontStyle: FontStyle.italic),
                      ),
                      Spacer(),
                      IconButton(
                        onPressed: () async {
                          // await DatabaseService().bookmarkJob(
                          //     context
                          //         .read<AuthenticationProvider>()
                          //         .employeeModel!
                          //         .id,
                          //     widget.jobModel.id);
                        },
                        icon: Icon(Icons.bookmark_border_rounded),
                      )
                    ],
                  ),
                  SizedBox(height: size.height * 0.03),
                  Container(
                    height: size.height * 0.25,
                    color: Colors.transparent,
                    child: Stack(
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: size.height * 0.07),
                          height: double.maxFinite,
                          decoration: BoxDecoration(
                              color: Colors.grey.shade300,
                              borderRadius: BorderRadius.circular(10)),
                          child: Column(
                            children: [
                              Spacer(),
                              Spacer(),
                              Spacer(),
                              Text(widget.employeeModel.name,
                                  style: TextStyle(
                                      fontSize: 22,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black)),
                              Spacer(),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Spacer(),
                                  Text(
                                    widget.employeeModel.interest,
                                    style:
                                        TextStyle(fontWeight: FontWeight.w500),
                                  ),
                                  Spacer(),
                                  Text(
                                    "•",
                                    style: TextStyle(
                                        fontSize: 22,
                                        color: Colors.grey.shade500),
                                  ),
                                  Spacer(),
                                  Text(
                                    widget.employeeModel.yearsOfExp + " years",
                                    style:
                                        TextStyle(fontWeight: FontWeight.w500),
                                  ),
                                  Spacer(),
                                  Text(
                                    "•",
                                    style: TextStyle(
                                        fontSize: 22,
                                        color: Colors.grey.shade500),
                                  ),
                                  Spacer(),
                                  Text(
                                    DateFormat.yMMMd()
                                        .format(widget.employeeModel.dob)
                                        .toString(),
                                    style:
                                        TextStyle(fontWeight: FontWeight.w500),
                                  ),
                                  Spacer(),
                                ],
                              ),
                              Spacer()
                            ],
                          ),
                        ),
                        Align(
                            alignment: Alignment.topCenter,
                            child: CircleAvatar(
                              backgroundColor:
                                  Color.fromARGB(255, 183, 245, 160),
                              radius: 50,
                              child: Text(
                                widget.employeeModel.name[0].toUpperCase(),
                                style: TextStyle(
                                    fontSize: 40,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black),
                              ),
                            ))
                      ],
                    ),
                  ),
                  SizedBox(height: size.height * 0.03),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(width: 20),
                      typeWidget(
                          "Exp Salary",
                          "\$${widget.employeeModel.expectedSalary}",
                          Icons.attach_money_outlined,
                          Color(0xffDCB26F).withOpacity(0.6)),
                      Spacer(),
                      typeWidget(
                          "Previous Type",
                          widget.employeeModel.previousJob,
                          Icons.work_outline_rounded,
                          Color(0xffe2d3fe)),
                      Spacer(),
                      typeWidget("Interest", widget.employeeModel.interest,
                          Icons.chair_outlined, Color(0xffbae4f4)),
                      SizedBox(width: 20),
                    ],
                  ),
                  // SizedBox(height: size.height * 0.05),
                  // Text(
                  //   "Description",
                  //   style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
                  // ),
                  // SizedBox(height: size.height * 0.02),
                  // Text(widget.)
                ],
              ),
            ),
          ),
          if (isLoading)
            Center(
              child: CircularProgressIndicator(),
            )
        ],
      ),
    );
  }
}
