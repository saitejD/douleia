import 'package:cloud_firestore/cloud_firestore.dart';

class EmployerModel {
  final String uid;
  final String id;
  final String userType;
  final String companyName;
  final DateTime startedYear;
  final String noofEmployees;
  final String companyLocation;
  final String companyDescription;
  final String profilePic;
  final List<String> appliedPeople;
  final List<String> addedJobs;

  EmployerModel({
    required this.userType,
    required this.id,
    required this.companyName,
    required this.startedYear,
    required this.noofEmployees,
    required this.companyLocation,
    required this.companyDescription,
    required this.profilePic,
    required this.uid,
    required this.appliedPeople,
    required this.addedJobs,
  });

  factory EmployerModel.fromJson(Map<String, dynamic> json, String id) {
    print(json);
    return EmployerModel(
      companyName: json["companyName"],
      startedYear: (json["startedYear"] as Timestamp).toDate(),
      noofEmployees: json["noOfEmployees"],
      companyLocation: json["companyLocation"],
      companyDescription: json["companyDescription"],
      uid: json['uid'],
      userType: json['userType'],
      profilePic: json['profilePic'],
      appliedPeople: List<String>.from(json['appliedPeople']),
      addedJobs: List<String>.from(json['addedJobs']),
      id: id,
    );
  }

  Map<String, dynamic> toJson() => {
        "companyName": companyName,
        "startedYear": startedYear,
        "noOfEmployees": noofEmployees,
        "companyLocation": companyLocation,
        "companyDescription": companyDescription,
        'uid': uid,
        "userType": userType,
        'profilePic': profilePic,
        'appliedPeople': appliedPeople,
        'addedJobs': addedJobs,
        'id': id,
      };
}
