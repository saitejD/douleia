import 'package:cloud_firestore/cloud_firestore.dart';

class JobModel {
  final String companyName;
  final String jobTitle;
  final String location;
  final DateTime postedDate;
  final String salary;
  final String jobType;
  final String position;
  final String description;
  final String jobCategory;
  final String companyUid;
  final String id;

  final List<String> appliedPeople;

  JobModel({
    required this.companyName,
    required this.jobTitle,
    required this.location,
    required this.postedDate,
    required this.salary,
    required this.jobType,
    required this.position,
    required this.description,
    required this.companyUid,
    required this.appliedPeople,
    required this.jobCategory,
    required this.id,
  });

  factory JobModel.fromJson(Map<String, dynamic> json, String id) {
    return JobModel(
      companyName: json["companyName"],
      jobTitle: json["jobTitle"],
      location: json["location"],
      postedDate: (json["postedDate"] as Timestamp).toDate(),
      salary: json["salary"],
      jobType: json["jobType"],
      position: json["position"],
      description: json["description"],
      companyUid: json['companyUid'],
      jobCategory: json['jobCategory'],
      appliedPeople: List<String>.from(json['appliedPeople']),
      id: id,
    );
  }

  Map<String, dynamic> toJson() => {
        'companyName': companyName,
        'jobTitle': jobTitle,
        "location": location,
        "postedDate": postedDate,
        "salary": salary,
        "jobType": jobType,
        "position": position,
        "description": description,
        'companyUid': companyUid,
        'appliedPeople': appliedPeople,
        'jobCategory':jobCategory,
      };
}
