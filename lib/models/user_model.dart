class UserModel {
  final String uid;
  final String userType;

  UserModel({required this.uid, required this.userType});

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      UserModel(uid: json['uid'], userType: json['userType']);

  Map<String, dynamic> toJson() => {"uid": uid, "userType": userType};
}
