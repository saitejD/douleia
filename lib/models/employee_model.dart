import 'package:cloud_firestore/cloud_firestore.dart';

class EmployeeModel {
  final String uid;
  final String id;
  final String userType;
  final String name;
  final DateTime dob;
  final String previousJob;
  final String yearsOfExp;
  final String expectedSalary;
  final String profilePic;
  String interest;
  final List<String> bookMarks;
  final List<String> recentlyAppliedJobs;

  EmployeeModel({
    required this.userType,
    required this.name,
    required this.dob,
    required this.previousJob,
    required this.yearsOfExp,
    required this.expectedSalary,
    required this.interest,
    required this.profilePic,
    required this.uid,
    required this.bookMarks,
    required this.recentlyAppliedJobs,
    required this.id,
  });

  factory EmployeeModel.fromJson(Map<String, dynamic> json, String id) {
    print(json);
    return EmployeeModel(
      name: json["name"],
      dob: (json["dob"] as Timestamp).toDate(),
      previousJob: json["previousJob"],
      yearsOfExp: json["yearsofExp"],
      expectedSalary: json["expectedSalary"],
      interest: json['interest'],
      uid: json['uid'],
      userType: json['userType'],
      profilePic: json['profilePic'],
      bookMarks: List<String>.from(json['bookMarks']),
      recentlyAppliedJobs: List<String>.from(json["recentlyAppliedJobs"]),
      id: id
    );
  }

  Map<String, dynamic> toJson() => {
        "name": name,
        "dob": dob,
        "previousJob": previousJob,
        "yearsofExp": yearsOfExp,
        "expectedSalary": expectedSalary,
        'interest': interest,
        'uid': uid,
        'userType': userType,
        'profilePic': profilePic,
        'bookMarks': bookMarks,
        "recentlyAppliedJobs": recentlyAppliedJobs,
        "id": id,
      };
}
