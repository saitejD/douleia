import 'package:douleia/bloc/authentication_provider.dart';
import 'package:douleia/widgets/toastMessage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:provider/provider.dart';

class AuthenticationService {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  // AuthenticationService(this._firebaseAuth);
  // User? get user => _firebaseAuth.currentUser;
  // Stream<User?> get authStateChanges => _firebaseAuth.authStateChanges();
  final GoogleSignIn _googleSignIn = GoogleSignIn();

  Future<String?> singingInWithGoogle(BuildContext context) async {
    if (_googleSignIn.currentUser != null) {
      _googleSignIn.signOut();
      return null;
    }
    try {
      GoogleSignInAccount? googleSignInAccount = await _googleSignIn.signIn();

      GoogleSignInAuthentication googleSignInAuthentication =
          await googleSignInAccount!.authentication;

      AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleSignInAuthentication.accessToken,
        idToken: googleSignInAuthentication.idToken,
      );

      UserCredential authResult =
          await _firebaseAuth.signInWithCredential(credential);

      assert(await authResult.user!.getIdToken() != null);
      var auth = Provider.of<AuthenticationProvider>(context, listen: false);
      auth.setCurrentUserType();
      await Future.delayed(Duration(seconds: 2));

      User? currentUser = await _firebaseAuth.currentUser;
      return _googleSignIn.currentUser!.photoUrl;
    } catch (e) {
      print(e);
      toastMessage("something went wrong! try again");
      return null;
    }
  }

  Future<GoogleSignInAccount?> singingOutWithGoogle() async {
    try {
      GoogleSignInAccount? account = await _googleSignIn.signOut();
      return account;
    } catch (e) {
      print(e);
      toastMessage("something went wrong! try again");
    }
  }

  Future<User?> registerUser(
      String email, String password, BuildContext context) async {
    try {
      UserCredential userCredential = await _firebaseAuth
          .createUserWithEmailAndPassword(email: email, password: password);
      print("here ${_firebaseAuth}");
      var auth = Provider.of<AuthenticationProvider>(context, listen: false);
      auth.setCurrentUserType();
      await Future.delayed(Duration(seconds: 2));
      return userCredential.user;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
        toastMessage('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        toastMessage('An account already exists for that email.');
        print('The account already exists for that email.');
      }
    } catch (e) {
      print(e);
    }
  }

  //SIGN IN METHOD
  Future<User?> signIn(
      String email, String password, BuildContext context) async {
    try {
      UserCredential userCredential = await _firebaseAuth
          .signInWithEmailAndPassword(email: email, password: password);

      var auth = Provider.of<AuthenticationProvider>(context, listen: false);
      auth.setCurrentUserType();
      await Future.delayed(Duration(seconds: 2));
      return userCredential.user;
    } on FirebaseAuthException catch (e) {
      print(e.code);
      if (e.code == "wrong-password") {
        toastMessage("invalid password");
        print(e);
      } else if (e.code == "user-not-found") {
        toastMessage("no user found with this email");
      }
    } catch (e) {
      toastMessage("Something went wrong! Try again");
      print(e);
    }
  }

  //SIGN OUT METHOD
  Future<bool> signOut() async {
    await _firebaseAuth.signOut();

    print('signout');
    return true;
  }
}
