import 'package:douleia/models/employee_model.dart';
import 'package:douleia/models/employer_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:douleia/models/job_model.dart';
import 'package:douleia/models/user_model.dart';

final FirebaseFirestore ins = FirebaseFirestore.instance;

class DatabaseService {
  final CollectionReference _usersCollection = ins.collection('users');
  final CollectionReference _employeeCollection = ins.collection('employee');

  final CollectionReference _employerCollection = ins.collection('employer');

  final CollectionReference _jobsCollection = ins.collection('jobs');

  // users addition

  Future<void> addUser(UserModel model) async {
    try {
      DocumentReference docRef = await _usersCollection.add(model.toJson());
    } catch (e) {
      print(e);
    }
  }

  Future<void> addEmployee(EmployeeModel model) async {
    try {
      DocumentReference docRef = await _employeeCollection.add(model.toJson());
    } catch (e) {
      print(e);
    }
  }

  Future<bool> updateEmployee(String id, EmployeeModel model) async {
    try {
      await _employeeCollection.doc(id).update(model.toJson());
      return true;
    } catch (e) {
      print(e);
    }
    return false;
  }

  Future<bool> addEmployer(EmployerModel model) async {
    try {
      DocumentReference docRef = await _employerCollection.add(model.toJson());
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> updateEmployer(String id, EmployerModel model) async {
    try {
      await _employerCollection.doc(id).update(model.toJson());
      return true;
    } catch (e) {
      print(e);
    }
    return false;
  }

  // user retrival

  Future<String?> getUserType(String uid) async {
    QuerySnapshot<Object?> snapshot =
        await _usersCollection.where('uid', isEqualTo: uid).get();
    // print("usertype");
    // print(snapshot.docs.length > 0);
    // print(snapshot.docs.first['userType']);
    return snapshot.docs.first.exists ? snapshot.docs.first['userType'] : null;
  }

  Future<EmployeeModel> getEmployee(String uid) async {
    return _employeeCollection
        .where('uid', isEqualTo: uid)
        .snapshots()
        .map((event) => event.docs
            .map((e) =>
                EmployeeModel.fromJson(e.data()! as Map<String, dynamic>, e.id))
            .first)
        .first;
  }

  Future<EmployerModel> getEmployer(String uid) async {
    return _employerCollection
        .where('uid', isEqualTo: uid)
        .snapshots()
        .map((event) => event.docs
            .map((e) =>
                EmployerModel.fromJson(e.data()! as Map<String, dynamic>, e.id))
            .first)
        .first;
  }

  // employee queries

  Future<void> applyJob(String jobId, String uid) async {
    try {
      await FirebaseFirestore.instance.runTransaction((transaction) async {
        DocumentSnapshot snapshot =
            await transaction.get(_jobsCollection.doc(jobId));
        if (snapshot.exists) {
          List<String> ap = List<String>.from(snapshot['appliedPeople']);
          print(ap);
          ap.add(uid);
          transaction.update(_jobsCollection.doc(jobId), {"appliedPeople": ap});
        }
      });
      await FirebaseFirestore.instance.runTransaction((transaction) async {
        DocumentSnapshot snapshot =
            await transaction.get(_employeeCollection.doc(uid));
        if (snapshot.exists) {
          List<String> rj = List<String>.from(snapshot['recentlyAppliedJobs']);
          print(rj);
          rj.add(jobId);
          transaction.update(_jobsCollection.doc(jobId), {"appliedPeople": rj});
        }
      });
    } catch (e) {
      print(e);
    }
  }

  Future<List<JobModel>> jobsForYou(String uid, String category) async {
    try {
      print("uid $category");
      final snapshot = await _jobsCollection
          .where("jobCategory", isEqualTo: category)
          .where("appliedPeople",whereNotIn: [uid])
          .get();
      if (snapshot.docs.isNotEmpty) {
        // print(snapshot.docs[1]);
        List<JobModel> jobs = snapshot.docs
            .map((event) => JobModel.fromJson(
                event.data()! as Map<String, dynamic>, event.id))
            .toList();
        print("jobsforyou ${jobs[0].jobTitle}");
        jobs.removeWhere((element) => element.id == uid);
        print(jobs.length);

        return jobs;
      }
    } catch (e) {
      print(e);
    }
    return [];
  }

  Future<List<List<JobModel>>> typesOfJobs() async {
    List<List<JobModel>> typesOfJobs = [[], [], []];
    try {
      final remote =
          await _jobsCollection.where("jobType", isEqualTo: "Remote").get();
      if (remote.docs.isNotEmpty) {
        typesOfJobs[0].addAll(remote.docs
            .map((e) =>
                JobModel.fromJson(e.data() as Map<String, dynamic>, e.id))
            .toList());
      }
      final fulltime =
          await _jobsCollection.where("jobType", isEqualTo: "Fulltime").get();
      if (fulltime.docs.isNotEmpty) {
        typesOfJobs[1].addAll(fulltime.docs
            .map((e) =>
                JobModel.fromJson(e.data() as Map<String, dynamic>, e.id))
            .toList());
      }
      final parttime =
          await _jobsCollection.where("jobType", isEqualTo: "Parttime").get();
      if (parttime.docs.isNotEmpty) {
        typesOfJobs[2].addAll(parttime.docs
            .map((e) =>
                JobModel.fromJson(e.data() as Map<String, dynamic>, e.id))
            .toList());
      }
      print("types of jobs ${typesOfJobs[1][0].jobTitle}");
      return typesOfJobs;
    } catch (e) {
      print(e);
    }
    return [];
  }

  Future<List<JobModel>> recentlyApplidJobs(String uid) async {
    try {
      final snapshot = await _jobsCollection
          .where("appliedPeople", arrayContains: uid)
          .get();
      print("snap ${snapshot.docs.length}");
      if (snapshot.docs.isNotEmpty) {
        List<JobModel> jobs = snapshot.docs
            .map((event) => JobModel.fromJson(
                event.data()! as Map<String, dynamic>, event.id))
            .toList();
        print("snap ${jobs.length}");
        return jobs;
      }
    } catch (e) {
      print(e);
    }
    return [];
  }

  Future<bool> bookmarkJob(String employeeId, String jobId) async {
    try {
      FirebaseFirestore.instance.runTransaction((transaction) async {
        DocumentSnapshot snapshot =
            await transaction.get(_employeeCollection.doc(employeeId));
        print(jobId);
        if (snapshot.exists) {
          List<String> bm = List<String>.from(snapshot['bookMarks']);

          bm.add(jobId);
          print(bm);
          transaction
              .update(_employeeCollection.doc(employeeId), {"bookMarks": bm});
          return true;
        }
      });
    } catch (e) {
      print(e);
    }
    return false;
  }

  // employer queries
  Future<void> addJob(JobModel model) async {
    try {
      DocumentReference docRef = await _jobsCollection.add(model.toJson());
    } catch (e) {
      print(e);
    }
  }

  Future<List<EmployeeModel>> peopleApplied(String uid) async {
    List<EmployeeModel> peopleApplied = [];
    try {
      final snapshots =
          await _jobsCollection.where("companyUid", isEqualTo: uid).get();
      print("people applied ${snapshots.docs.length}");
      if (snapshots.docs.isNotEmpty) {
        List<String> people = [];
        snapshots.docs.forEach((element) {
          people
              .addAll(List<String>.from(element.get("appliedPeople")).toList());
        });
        snapshots.docs.map((e) {
          print("e ${e.get("appliedPeople")}");
          people.addAll(e.get("appliedPeople"));
        });
        print("people applied $people");
        if (people.isNotEmpty) {
          final snap =
              await _employeeCollection.where("uid", whereIn: people).get();
          if (snap.docs.isNotEmpty) {
            peopleApplied.addAll(snap.docs
                .map((e) => EmployeeModel.fromJson(
                    e.data() as Map<String, dynamic>, e.id))
                .toList());
            print("People Data ${peopleApplied[0].interest}");
          }
        }
      }
    } catch (e) {
      print(e);
    }

    return peopleApplied;
  }

  Future<List<JobModel>> recentlyPostedJobs(String uid) async {
    List<JobModel> recentJobs = [];
    try {
      final snapshots =
          await _jobsCollection.where("companyUid", isEqualTo: uid).get();
      if (snapshots.docs.isNotEmpty) {
        recentJobs.addAll(snapshots.docs
            .map((e) =>
                JobModel.fromJson(e.data() as Map<String, dynamic>, e.id))
            .toList());
      }
    } catch (e) {
      print(e);
    }
    return recentJobs;
  }

  Future<void> deleteJob(String id) async {
    try {
      await _jobsCollection.doc(id).delete();
    } catch (e) {
      print(e);
    }
  }

  Future<void> updateJob(String id, JobModel model) async {
    try {
      await _jobsCollection.doc(id).update(model.toJson());
    } catch (e) {
      print(e);
    }
  }

  
}
