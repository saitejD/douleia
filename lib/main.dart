import 'package:douleia/bloc/authentication_provider.dart';
import 'package:douleia/constants/app_theme.dart';
import 'package:douleia/firebase_services/authentication_service.dart';
import 'package:douleia/firebase_services/database_service.dart';
import 'package:douleia/models/user_model.dart';
import 'package:douleia/screens/employee/choose_category.dart';
import 'package:douleia/screens/employee/employee_details.dart';
import 'package:douleia/screens/employee/employee_home.dart';
import 'package:douleia/screens/employer/employer_home.dart';
import 'package:douleia/screens/home.dart';
import 'package:douleia/screens/login.dart';
import 'package:douleia/screens/register.dart';
import 'package:douleia/screens/splash.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  SystemChrome.setSystemUIOverlayStyle(
    const SystemUiOverlayStyle(statusBarColor: Colors.transparent),
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        // Provider<AuthenticationService>(
        //   create: (_) => AuthenticationService(),
        // ),

        ChangeNotifierProvider<AuthenticationProvider>(
          create: (_) => AuthenticationProvider(),
        ),

        StreamProvider(
          create: (context) => context.read<AuthenticationProvider>().authState,
          initialData: null,
        )
      ],
      child: MaterialApp(
        title: 'Douleia',
        debugShowCheckedModeBanner: false,
        theme: AppThemeData.themeData,
        // home: EmployeDetailsScreen(),
        initialRoute: '/',
        routes: {
          '/': (contex) => const SplashScreen(),
          '/auth': (context) => Consumer<AuthenticationProvider>(
                builder:
                    (context, AuthenticationProvider authProvider, child) =>
                        const AuthenticationWrapper(),
              ),
        },
      ),
    );
  }
}

class AuthenticationWrapper extends StatefulWidget {
  const AuthenticationWrapper({Key? key}) : super(key: key);

  @override
  State<AuthenticationWrapper> createState() => _AuthenticationWrapperState();
}

class _AuthenticationWrapperState extends State<AuthenticationWrapper> {
  // late User? firebaseUser;
  // late String? _userType;
  // @override
  // void initState() {
  //   super.initState();
  // }

  // @override
  // void didChangeDependencies() async {
  //   firebaseUser = context.watch<AuthenticationProvider>().user;
  //   _userType = context.read<AuthenticationProvider>().userType;
  //   // if (firebaseUser != null) {
  //   //   _userType =
  //   //       await context.read<DatabaseService>().getUserType(firebaseUser!.uid);
  //   // }
  //   // setState(() {});
  //   super.didChangeDependencies();
  // }

  @override
  Widget build(BuildContext context) {
    final User? firebaseUser = context.watch<AuthenticationProvider>().user;

    // print("firebaseUser is $firebaseUser");
    if (firebaseUser != null) {
      final String? _userType = context.read<AuthenticationProvider>().userType;
      // print("object $_userType");
      // context.read<DatabaseService>().getUserType(firebaseUser.uid);
      var auth = Provider.of<AuthenticationProvider>(context, listen: false);
      auth.setUser();
      if (_userType == 'employee') {
        return const EmployeeHomeScreen();
      } else if (_userType == 'employer') {
        return const EmployerHomeScreen();
      }
    }
    return const LoginScreen();
  }
}
