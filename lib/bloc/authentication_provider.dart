import 'package:douleia/firebase_services/authentication_service.dart';
import 'package:douleia/firebase_services/database_service.dart';
import 'package:douleia/models/employee_model.dart';
import 'package:douleia/models/employer_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';

class AuthenticationProvider extends ChangeNotifier {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final DatabaseService _databaseService = DatabaseService();

  User? user;
  EmployerModel? employerModel;
  EmployeeModel? employeeModel;
  String? userType;

  //Using Stream to listen to Authentication State
  Stream<User?> get authState => _firebaseAuth.authStateChanges();

  User get currentUser => user!;

  // AuthenticationProvider();

  Future<User?> setCurrentUserType() async {
    user = _firebaseAuth.currentUser;
    if (user != null) {
      userType = await _databaseService.getUserType(user!.uid);
      print("userType");
      return user;
      // if (userType!.isNotEmpty) {
      //   if (userType == 'employee') {
      //     employeeModel = await _databaseService.getEmployee(user!.uid);
      //   } else {
      //     employerModel = await _databaseService.getEmployer(user!.uid);
      //     print("name: ${employerModel!.name}");
      //   }
      // }
    }
    // notifyListeners();
  }

  Future<void> setUser() async {
    if (userType!.isNotEmpty) {
      if (userType == 'employee') {
        employeeModel = await _databaseService.getEmployee(user!.uid);
      } else {
        employerModel = await _databaseService.getEmployer(user!.uid);
        print("name: ${employerModel!.companyName}");
      }
    }
  }
}