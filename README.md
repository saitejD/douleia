# Douleia

It is a Job Portal Application where you can find your dream job. Recruiters directly contact you when you submit your interest. It is supported by Firebase as a Backend Servicer.

# App UI

<img src="ui/Screenshot_1663387408.png" width="300"></br>

<img src="ui/Screenshot_1663388172.png" width="300"></br>

<img src="ui/Screenshot_1663387794.png" width="300"></br>

<img src="ui/Screenshot_1663387883.png" width="300"></br>

<img src="ui/Screenshot_1663387805.png" width="300"></br>

<img src="ui/Screenshot_1663387926.png" width="300"></br>

<img src="ui/Screenshot_1663387939.png" width="300"></br>

<img src="ui/Screenshot_1663387952.png" width="300"></br>
